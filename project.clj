(defproject clominal "1.0.0-SNAPSHOT"
  :description "Clominal will be a clojure terminal constructed by Swing components."
  :dependencies [[org.clojure/clojure "1.4.0"]
                 [org.clojure/clojure-contrib "1.2.0"]
                 [org.clojure/tools.logging "0.4.1"]
                 [org.apache.logging.log4j/log4j-api "2.11.2"]
                 [org.apache.logging.log4j/log4j-core "2.11.2"]
                 [com.fifesoft/rsyntaxtextarea "2.0.7"]
                 [com.weblookandfeel/weblaf-ui "1.2.8"]]
  :main clominal.core)
